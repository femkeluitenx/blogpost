
#**CMD Design Challenge 1**

####**Onderzoek**

Voor de Design Challenge moeten we eerstejaars studenten van de Hogeschool Rotterdam met elkaar verbinden door middel van een online/offline spel. De doelgroep de we hebben gekozen zijn mensen die affiniteit hebben met de Rotterdamse kunst en musea en daarnaast in bezit zijn van een Museumpas. De reden dat wij voor deze doelgroep hebben gekozen is omdat we het een uitdagende doelgroep vinden en ervoor willen zorgen dat de Rotterdamse kunstscene wordt vergroot en meer aandacht krijgt. In Rotterdam zijn er vele bezienswaardigheden die studenten met Rotterdam verbinden en zo ook mensen bij elkaar kunnen brengen.

Om zoveel mogelijk over de doelgroep te weten kunnen komen gaan we de eerstejaars studenten interviewen door middel van vragen en onderzoek. Omdat de kunstsector in Rotterdam vrij breed is, heeft iedereen andere voorkeuren of interesses, daarom willen wij dit zoveel mogelijk verzamelen en opnemen. Hierdoor willen we een beeld gaan creëren van de doelgroep waarmee we een prototype van ons spel kunnen maken.


####**Spel idee**
Voor de game willen we een spel gaan creëren wat gebaseerd is op de Rotterdamse kunst. Hierbij kun je denken aan vragen over bepaalde artiesten/kunstwerken of een kunstwerk proberen na te maken door middel van een tekening. Via deze manier van ter werk gaan kan je punten verdienen. De puntentelling wordt bijgehouden via het spel zelf, en worden verdeeld in groepen binnen het gekozen thema, kunst. Tijdens het spelen van de game kan je onderling chatten met andere spelers die binnen jouw groep vallen, om zo elkaar beter te leren kennen. Hoe meer punten je verdient, hoe meer pop-ups je krijgt. Via deze groepen proberen wij leuke evenementen/bezienswaardigheden met elkaar te koppelen zodat zij met elkaar op pad kunnen gaan en daardoor in contacten komen met elkaar en de stad Rotterdam. 

De offline game is in combinatie met de online game. Doordat de groepen met elkaar naar de evenementen/bezienswaardigheden gaan, willen we daar een interactieve game van gaan maken. 


####**Taakverdelingen**
Om zo efficicient mogelijk te werken, hebben we het onderzoek gesplitst in ons team. Hierdoor kunnen we allemaal onze volledige aandacht besteden aan ieder z’n eigen onderwerp, waardoor we het beste eruit kunnen halen.

####**Reflectie**
We bedachten allereerst een aantal specifieke deelonderwerpen die typerend zijn voor het onderzoek. Mijn invulling in dit onderzoek ging over verschillende soorten kunst en kunstenaars. Breed onderwerp naar eerste idee. Maar vanwege het feit dat we de kunst naar Rotterdam getrokken hadden maakte het al iets minder breed.
Wat voor verschillende soorten kunst bestaan er eigenlijk in Rotterdam vroeg ik mezelf af. In mijn notitieboekje noteerde ik de musea/kunst waarmee ik al bekend was. Daarna gebruikte ik het internet voor een uitgebreider antwoord op mijn vraag. Ik kwam  eigenlijk tot de conclusie dat er veel uiteenlopende soorten kunst in Rotterdam aanwezig zijn. Voornamelijk in de musea, dus letterlijk overnemen tot wat voor tijd/cultuur een kunstwerk, bijvoorbeeld in het openbaar behoort, leek me niet van belang.  Ik ben zeker wat wijzer geworden over mijn onderzoek, het meeste wist ik eigenlijk al omdat ik van nature creatief ingesteld ben, en daarom een zekere interesse heb voor kunst en kunstenaars. Up-to-date te blijven betreft expositie's en upcoming artiesten is voor mij een must.

Veel wist ik nog niet, bijvoorbeeld wat voor doel kunst in onze samenleving heeft. Na het lezen van verschillende artikelen betreft dit onderwerp ben ik mezelf er meer van bewust dat dit aspect van onze cultuur noodzakelijk is in onze samenleving. Misschien iets uit de richting van het ontwikkelen van de game maar toch een interessante kwestie naar mijn mening. 

Lastig vond ik het vinden naar een juist antwoord omdat ik er lichtelijk moeite mee had om een spel te koppelen aan de informatie. 

Ik heb anderen in mijn groepje het geschreven stukje tekst te laten lezen, en heb ook om feedback gevraagd. Ik maakte uit de reacties op dat ik het iets te globaal heb aangepakt en dat ik de volgende keer iets meer rekening houden met het uitgangspunt.






> Written with [StackEdit](https://stackedit.io/).