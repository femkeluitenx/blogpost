#**Design**
---

*Allereerst kwamen er aantal filosofische quotes aan bod van verschillende auteurs, met als voorbeeld van wat ik me kan herinneren, dat eigenlijk alles om je heen design is.*
 
Design wordt gezien als een creatief proces waarbij extra waarde, betekenis en kwaliteit aan communicatie, een product, omgeving of ervaring wordt toegevoegd. Die toegevoegde waarde biedt de gebruiker de mogelijkheid door aanschaf, bezit en beleving van een product of dienst zich te onderscheiden. Doel is het gebruik te bevorderen of te verbeteren. Design beïnvloed niet alleen de functie, maar ook de beleving van en identificatie met een product.

Naast de definitie van design behandelde de spreker ook wat voor andere soorten design er bestaan: Graphic design, information design, corporate design, brand design en web design zijn gericht op het vinden van oplossingen voor communicatie en het overdragen van informatie. Interaction design, experience design, industrial design en fashion design zijn meer gericht op producten.

#####**Ontwerpproces**

Ook werd het ontwerp proces behandelt:

Stap 1: het onderzoeken van de opdracht.
Stap 2: Concept opstellen.
Stap 3: Ontwerpen
Stap 4: Prototypen
Stap 5: Testen en resultaten analyseren.

#####**Is kunst design?**
Nee. Design is instrumenteel en staat niet op zichzelf zoals kunst. Design voegt waarde toe aan een product. Kunst is het product zelf. Design wordt wel omschreven als toegepaste kunst. De overgang tussen beide vakgebieden is vloeiend. Sommige designers laten zich graag inspireren door beeldende kunstenaars en omgekeerd.

----



###**Reflectie**

Aan de hand van mijn vorige opleiding Grafisch Vormgeven,  weet ik al vrij veel van Design af. Ik ben er 4 jaar mee bezig geweest. Dit college zorgde voor een korte heads-up! Ook heb ik deze informatie benut in ons project.


> Written with [StackEdit](https://stackedit.io/).