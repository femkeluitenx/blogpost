
###**DONDERDAG**

Het studie-coaching uur bestond uit een opsomming van eventuele studiebelemmeringen, moeilijkheid etc. Mocht ik problemen ondervinden dan kan ik gemakkelijk contact zoeken met een luisterend oor. Momenteel ondervind ik nog geen moeilijkheden en is zelfstudie nog vrij nieuw. Ik moet de problemen nog tegenkomen. Wel heb ik een beetje een onwetend gevoel zo nu en dan. Omdat ik niet kan pijlen of ik in de goede richting zit met mijn gedachtegangen, werk en aanpak.
Meestal neem ik hoogte van mijn groepje en stel dan vast of ik het anders moet aanpakken ja of nee. Ik weet dat daar ook coaches voor zijn maar omdat we nu nog zo pril in de iteratie zitten wacht ik het nog even af. Ook stelt het me gerust dat het behalen van deadlines voor nu nog geen must is. Het gaat nu erg om ontplooien, onderzoeken, kennismaken en bestuderen van verschillende aspecten binnen de studie CMD. Er word graag een proces gezien met problemen die je aantreft en fouten die je maakt.  Je leert namelijk het meest van fouten!

Na de studie coaching, gingen we als groepje door om nog een plan van aanpak te maken voor komende week, en de stand-up van aanstaande maandag. Ook hebben we overleg gehad over ieders onderzoek en hebben we al een klein concept opgesteld. Ons concept is eigenlijk een mix van de apps Tinder en Trivia met opkomende pop-ups van musea. Als doel heeft het spel mensen te verbinden binnen de doelgroep kunstliefhebbers met een museumkaart. We moeten nog enquête gegevens analyseren om een definitief concept te ontwikkelen.

Ook heb ik een klein begin gemaakt aan het logo voor onze game, wat tevens de taak van Janice was. Het ontwerp proces van woensdag was hier weer van toepassing.  Hier ondervond ik wel een probleem: de naam van de game! Tijdens onze brainstorm sessie van vanmiddag kwamen we met zn allen op interessante ingevingen van eventuele namen voor de game. Alleen over het kiezen van een juiste naam kwamen we niet over uit. Tja, dat maakt het ontwerpen van een logo ook een stukje moeilijker natuurlijk. 

Als laatste had ik de taak een visuele weergave te maken voor onze stand-up. Mijn plan is om het weer te geven zoals woensdag, tijdens het werkcollege over Design waarin we een weergave moesten illustreren van een willekeurig ontwerp proces.




> Written with [StackEdit](https://stackedit.io/).