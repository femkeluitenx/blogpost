
##**Today**

Maandag, een opstart-dag van vele, aldus voor mij!

Vandaag begonnen we de dag met een korte stand-up van onze stand van zaken. We pitchte aan de hand van een korte presentatie op papier (in iteratie's weergegeven) onze gedachte over doelgroep, thema, onderzoek en concept (ging super!). We kregen als feedback op onze stand-up dat we te afwachtend waren met de resultaten van de enquête.

Aan de hand van de feedback van vanochtend zijn we (Leroy en ik) op pad gegaan voor fieldresearch over onze doelgroep bij de WDKA. Daar hebben we een aantal mensen ge-interviewt met dezelfde vragen als in de enquête.  We hebben niet heel veel kandidaten kunnen vinden wegens het tijdstip waarop we op locatie waren. Veel studenten hadden voornamelijk les.

Terug in de studio hebben we de afgenomen interviews geanalyseerd en vergeleken met elkaar. Daaruit hebben we (gezamenlijk) conclusies getrokken en de kernwoorden geïmplementeerd in ons concept. 


#####**Werkcollege blog**
Aan de hand van een korte vervolg presentatie over het schrijven van een blog hebben we de opdracht gekregen het blog in het programma Markdown te uploaden naar Gitlab. Allereerst was de bedoeling een account te maken. Vervolgens moest je je blog installeren aan de hand van instructies op deze website: bit.do/bloggen. Na de tutorial kon je daadwerkelijk aan de slag met het uploaden van je markdown bestanden!

Ik vond het fijn dat er een stapsgewijze uitleg aanwezig was,  tevens visueel weergeven. Het was even goed lezen en begrijpen waar wat stond in het programma, maar de uitvoering ging eigenlijk vrij gemakkelijk. Ik had er wel moeite mee een .md bestand te uploaded, omdat ik de post niet zag verschijnen, ook na een aantal keer klooien niet. Ik ben daarom opnieuw begonnen, en heb eigenlijk geen hulp gevraagd want ik denk dat ik misschien iets te gehaast heb gelezen. 
Dus ik kijk de tweede keer proberen aan en als het dan weer niet lukt, vraag ik om feedback!


> Written with [StackEdit](https://stackedit.io/).