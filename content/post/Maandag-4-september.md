##**Maandag 4 september**

Vandaag was mijn eerste lesdag op de Hogeschool! We begonnen met een hoorcollege over Games. Hier werden de basisprincipe's van een game uitgelegd. Een game bestaat uit een doel, een stel regels en uit keuzes. Deze factoren maken een game, een ''Gameplay''. _


###**_Gameplay_**

Het zogezegde doel van een gameplay is drempels overwinnen, een belevenis en-/of ervaring meegeven en een context bieden. Dit word ook wel een ''Meaningful play'' genoemd. Een meaningful play ontstaat door interactie in het systeem en moet leuk en interessant blijven voor de gebruiker om het te willen blijven spelen.

Naast een doel bied een game ook een aantal keuzes voor de gebruiker. Ook wel, merkbare resultaten op basis van actie van de speler. Hieronder te verstaan: level up,  punten behalen en audio en-/of visuele feedback. 

Het derde, tevens laatste kopje van het kopje gameplay is: Regels.
Regels beperken spelers actie's en zijn dus van belang in een game. Hieronder een kleine opsomming van bijbehorende eigenschappen:

	1. Expliciteit en duidelijk
	2. Regels staan vast
	3. Herhaling
	4. Behoren toe aan elke speler
	5. Zijn bindend^
	
	Deze regels gelden in de: ''Magic Circle'' oftewel, het gebied. 

##**Reflectie**
Aan de hand van dit hoorcollege, ben ik meer te weten gekomen over de definitie van games. Dit was erg nuttig voor onze Design Challenge waarbij we een game moeten ontwikkelen. Ik denk dat deze les me erg geholpen heeft om een beginnende kennis te ontwikkelen van de term multimedia design. Het was een erg duidelijke voordracht en stimuleerde me erg om aan de Design Challenge te beginnen! 
	


> Written with [StackEdit](https://stackedit.io/).