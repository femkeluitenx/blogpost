
##**Woensdag**


We begonnen de dag met een werkcollege over Design. De opdracht was om individueel een project te kiezen en die visueel volgens het design proces op papier te zetten. In deze volgorde gingen we ter werk:

 - Huidige situatie
 - Ontwerpproces = Vragen > onderzoeken > concept > prototype 
 - Gewenste situatie
 
 Ik koos voor een schematische weergave van een ontwerp voor een t-shirt logo. Het ging me eigenlijk vrij gemakkelijk af omdat ik door mijn grafische achtergrond heel makkelijk dingen kan visualiseren. 
 Daarna kozen we binnen ons groepje een compositie die we wilde verbeteren, want dat was uiteraard de opdracht die de coaches ons gaven. Mijn compositie namen we onder de loep en gingen eigenlijk alleen opzoek naar het ''Wicked Problem'' en de ''Solution'' in het proces, omdat dat nog niet helemaal zichtbaar was. Tevens was dat ook moeilijk te bepalen want de markt verandert constant. Uiteindelijk hadden we als groep een mooie weergave ontworpen en presenteerde we dat aan de klas. Als feedback ontvingen we dat er een visueel aantrekkelijke poster was ontstaat en dat er, zoals ik al aangaf, geen duidelijk probleem aanwezig was. Stel die was wel aanwezig dan had je de cyclus rond kunnen maken. 
 
We hebben daarna onze individuele onderzoeken afgerond, aan de hand daarvan hebben we doelvragen opgesteld voor het interview.  Ook hebben we een taakverdeling gemaakt voor de rest van de week. Mijn taak was om een logo te ontwikkelen voor de game en naam van ons groepje. 

Allereerst heb ik een mind-map gemaakt van de naam van ons groepje: blokparty. Daarna heb ik gekeken welk onderwerp van de mind-map het best te vertalen was in een beeld. Ik ging aan de hand van het onderwerp van de mind-map op onderzoek uit naar inspiratie.  Toen ben ik begonnen met het maken van schetsen en uiteindelijk heb ik ook verschillende definitieve ontwerpen gemaakt.

Als groepje kozen we uit een logo dat we het best bij onszelf vonden passen. Dit was een logo opgebouwd uit lego blokjes, wat ''building blocks'' als achterliggende gedachten heeft. Ook heeft het relatie met vroeger, omdat het toen een echte hype was! Dit logo was door Janice gemaakt, die had namelijk dezelfde taak als ik!



####**WORKSHOP BLOG**

Ook kregen we vandaag een kick-off van het opstellen van een blog. 
Hier kwam de definitie en de basisprincipe van een .html bestand aan bod. Html staat voor: HyperText Markup Language. Het is de standaard opmaaktaal voor wegpagina's en heeft als doel structuur te geven aan codes. Het blog gaan we bijhouden in een markdown programma genaamd Stackedit. Markdown is een lichtgewicht opmaaktaal op basis van platte tekst die ontworpen is dat het gemakkelijk valt te converteren naar HTML en andere formaten. 

Deze informatie was super nuttig voor mij! In mijn vorige studie heb ik namelijk les gehad in het opzetten van een wesbite in html en css. Maar 1 periode weliswaar, dus je kan spreken van een basis kennis. Ik vond het super interessant om het schrijven van "code" weer op te pakken en er meer over te weten te komen.  Ik hoop dat we in de toekomst meer les krijgen in het echte code schrijven.  Het programma Markdown ging me gemakkelijk af omdat de structuur van de tekst duidelijk was aangegeven.



> Written with [StackEdit](https://stackedit.io/).